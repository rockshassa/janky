
class Tests

	def self.unitTestA
		return <<XMLDELIMITER
		<?xml version='1.0' encoding='UTF-8'?>
<project>
  <actions/>
  <description>Runs GrubKit Unit Tests in a DEBUG build off the specified branch</description>
  <logRotator class="hudson.tasks.LogRotator">
    <daysToKeep>5</daysToKeep>
    <numToKeep>5</numToKeep>
    <artifactDaysToKeep>-1</artifactDaysToKeep>
    <artifactNumToKeep>-1</artifactNumToKeep>
  </logRotator>
  <keepDependencies>false</keepDependencies>
  <properties>
    <jenkins.plugins.hipchat.HipChatNotifier_-HipChatJobProperty plugin="hipchat@0.1.5">
      <room>T-Rex Nation</room>
      <startNotification>true</startNotification>
      <notifySuccess>true</notifySuccess>
      <notifyAborted>false</notifyAborted>
      <notifyNotBuilt>false</notifyNotBuilt>
      <notifyUnstable>false</notifyUnstable>
      <notifyFailure>true</notifyFailure>
      <notifyBackToNormal>true</notifyBackToNormal>
    </jenkins.plugins.hipchat.HipChatNotifier_-HipChatJobProperty>
    <org.jenkinsci.plugins.mavenrepocleaner.MavenRepoCleanerProperty plugin="maven-repo-cleaner@1.2">
      <notOnThisProject>false</notOnThisProject>
    </org.jenkinsci.plugins.mavenrepocleaner.MavenRepoCleanerProperty>
    <hudson.model.ParametersDefinitionProperty>
      <parameterDefinitions>
        <hudson.model.ChoiceParameterDefinition>
          <name>Build</name>
          <description></description>
          <choices class="java.util.Arrays$ArrayList">
            <a class="string-array">
              <string>Debug</string>
              <string>Release</string>
            </a>
          </choices>
        </hudson.model.ChoiceParameterDefinition>
        <hudson.model.ChoiceParameterDefinition>
          <name>Branch</name>
          <description></description>
          <choices class="java.util.Arrays$ArrayList">
            <a class="string-array">
              <string>integration</string>
              <string>ng_UnitTester</string>
            </a>
          </choices>
        </hudson.model.ChoiceParameterDefinition>
        <org.jvnet.jenkins.plugins.nodelabelparameter.NodeParameterDefinition plugin="nodelabelparameter@1.5.1">
          <name>node</name>
          <description>target the iOS slave node specifically
it is launched via JNLP/java web start and has permission 
to boot/interact with the simulator</description>
          <allowedSlaves>
            <string>iOS Sim Slave Node</string>
            <string>nick_qa_laptop</string>
          </allowedSlaves>
          <defaultSlaves>
            <string>nick_qa_laptop</string>
          </defaultSlaves>
          <triggerIfResult>multiSelectionDisallowed</triggerIfResult>
          <allowMultiNodeSelection>false</allowMultiNodeSelection>
          <triggerConcurrentBuilds>false</triggerConcurrentBuilds>
          <ignoreOfflineNodes>false</ignoreOfflineNodes>
          <nodeEligibility class="org.jvnet.jenkins.plugins.nodelabelparameter.node.AllNodeEligibility"/>
        </org.jvnet.jenkins.plugins.nodelabelparameter.NodeParameterDefinition>
      </parameterDefinitions>
    </hudson.model.ParametersDefinitionProperty>
  </properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.2.7">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <url>ssh://git@stash.grubhubseamless.com:7999/mob/dinerapp-ios.git</url>
        <credentialsId>91cb79d8-7ef7-44c1-801a-f95c6f69a515</credentialsId>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/${BRANCH}</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"/>
    <extensions>
      <hudson.plugins.git.extensions.impl.AuthorInChangelog/>
    </extensions>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <triggers/>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.tasks.Shell>
      <command>pwd 
whoami

echo invoking xcode build

xcodebuild clean build test -scheme GrubKitTests &quot;CONFIGURATION_BUILD_DIR=${WORKSPACE}/build&quot; -project &quot;${WORKSPACE}/src/DinerApp.xcodeproj&quot; -destination &quot;platform=iOS Simulator,name=iPhone 6&quot; TEST_AFTER_BUILD=YES SL_RUN_UNIT_TESTS=YES

echo unit tests complete</command>
    </hudson.tasks.Shell>
    <hudson.plugins.python.Python plugin="python@1.2">
      <command>import requests

payload = {&apos;jobName&apos;:&apos;some_job_name&apos;,
			&apos;jobStatus&apos;:&apos;success&apos;}

r = requests.get(&apos;http://nick-mbp.local:2345&apos;, params=payload)

print r.url
</command>
    </hudson.plugins.python.Python>
  </builders>
  <publishers>
    <hudson.tasks.Mailer plugin="mailer@1.11">
      <recipients>ngalasso@grubhub.com</recipients>
      <dontNotifyEveryUnstableBuild>false</dontNotifyEveryUnstableBuild>
      <sendToIndividuals>false</sendToIndividuals>
    </hudson.tasks.Mailer>
    <jenkins.plugins.hipchat.HipChatNotifier plugin="hipchat@0.1.5">
      <authToken>9a1c7fa63d3e63b150dd322e8373ab</authToken>
      <buildServerUrl>http://jenkinsmob.grubhubseamless.com:8080/</buildServerUrl>
      <room>T-Rex Nation</room>
      <sendAs>Nicolas</sendAs>
    </jenkins.plugins.hipchat.HipChatNotifier>
  </publishers>
  <buildWrappers/>
</project>
XMLDELIMITER
	end

	def self.unitTestB
		return << XMLDELIMITER
		<?xml version='1.0' encoding='UTF-8'?>
<project>
  <actions/>
  <description>Runs GrubKit Unit Tests in a DEBUG build off the specified branch</description>
  <logRotator class="hudson.tasks.LogRotator">
    <daysToKeep>5</daysToKeep>
    <numToKeep>5</numToKeep>
    <artifactDaysToKeep>-1</artifactDaysToKeep>
    <artifactNumToKeep>-1</artifactNumToKeep>
  </logRotator>
  <keepDependencies>false</keepDependencies>
  <properties>
    <jenkins.plugins.hipchat.HipChatNotifier_-HipChatJobProperty plugin="hipchat@0.1.5">
      <room>T-Rex Nation</room>
      <startNotification>true</startNotification>
      <notifySuccess>true</notifySuccess>
      <notifyAborted>false</notifyAborted>
      <notifyNotBuilt>false</notifyNotBuilt>
      <notifyUnstable>false</notifyUnstable>
      <notifyFailure>true</notifyFailure>
      <notifyBackToNormal>true</notifyBackToNormal>
    </jenkins.plugins.hipchat.HipChatNotifier_-HipChatJobProperty>
    <org.jenkinsci.plugins.mavenrepocleaner.MavenRepoCleanerProperty plugin="maven-repo-cleaner@1.2">
      <notOnThisProject>false</notOnThisProject>
    </org.jenkinsci.plugins.mavenrepocleaner.MavenRepoCleanerProperty>
    <hudson.model.ParametersDefinitionProperty>
      <parameterDefinitions>
        <hudson.model.ChoiceParameterDefinition>
          <name>Build</name>
          <description></description>
          <choices class="java.util.Arrays$ArrayList">
            <a class="string-array">
              <string>Debug</string>
              <string>Release</string>
            </a>
          </choices>
        </hudson.model.ChoiceParameterDefinition>
        <hudson.model.ChoiceParameterDefinition>
          <name>Branch</name>
          <description></description>
          <choices class="java.util.Arrays$ArrayList">
            <a class="string-array">
              <string>integration</string>
              <string>ng_UnitTester</string>
            </a>
          </choices>
        </hudson.model.ChoiceParameterDefinition>
        <org.jvnet.jenkins.plugins.nodelabelparameter.NodeParameterDefinition plugin="nodelabelparameter@1.5.1">
          <name>node</name>
          <description>target the iOS slave node specifically
it is launched via JNLP/java web start and has permission 
to boot/interact with the simulator</description>
          <allowedSlaves>
            <string>iOS Sim Slave Node</string>
            <string>nick_qa_laptop</string>
          </allowedSlaves>
          <defaultSlaves>
            <string>nick_qa_laptop</string>
          </defaultSlaves>
          <triggerIfResult>multiSelectionDisallowed</triggerIfResult>
          <allowMultiNodeSelection>false</allowMultiNodeSelection>
          <triggerConcurrentBuilds>false</triggerConcurrentBuilds>
          <ignoreOfflineNodes>false</ignoreOfflineNodes>
          <nodeEligibility class="org.jvnet.jenkins.plugins.nodelabelparameter.node.AllNodeEligibility"/>
        </org.jvnet.jenkins.plugins.nodelabelparameter.NodeParameterDefinition>
      </parameterDefinitions>
    </hudson.model.ParametersDefinitionProperty>
  </properties>
  <scm class="hudson.plugins.git.GitSCM" plugin="git@2.2.7">
    <configVersion>2</configVersion>
    <userRemoteConfigs>
      <hudson.plugins.git.UserRemoteConfig>
        <url>ssh://git@stash.grubhubseamless.com:7999/mob/dinerapp-ios.git</url>
        <credentialsId>91cb79d8-7ef7-44c1-801a-f95c6f69a515</credentialsId>
      </hudson.plugins.git.UserRemoteConfig>
    </userRemoteConfigs>
    <branches>
      <hudson.plugins.git.BranchSpec>
        <name>*/${BRANCH}</name>
      </hudson.plugins.git.BranchSpec>
    </branches>
    <doGenerateSubmoduleConfigurations>false</doGenerateSubmoduleConfigurations>
    <submoduleCfg class="list"/>
    <extensions>
      <hudson.plugins.git.extensions.impl.AuthorInChangelog/>
    </extensions>
  </scm>
  <canRoam>true</canRoam>
  <disabled>false</disabled>
  <blockBuildWhenDownstreamBuilding>false</blockBuildWhenDownstreamBuilding>
  <blockBuildWhenUpstreamBuilding>false</blockBuildWhenUpstreamBuilding>
  <triggers/>
  <concurrentBuild>false</concurrentBuild>
  <builders>
    <hudson.tasks.Shell>
      <command>pwd 
whoami

echo invoking xcode build

xcodebuild clean build test -scheme GrubKitTests &quot;CONFIGURATION_BUILD_DIR=${WORKSPACE}/build&quot; -project &quot;${WORKSPACE}/src/DinerApp.xcodeproj&quot; -destination &quot;platform=iOS Simulator,name=iPhone 6&quot; TEST_AFTER_BUILD=YES SL_RUN_UNIT_TESTS=YES

echo unit tests complete</command>
    </hudson.tasks.Shell>
    <hudson.plugins.python.Python plugin="python@1.2">
      <command>import requests

payload = {&apos;jobName&apos;:&apos;some_job_name&apos;,
			&apos;jobStatus&apos;:&apos;success&apos;}

r = requests.get(&apos;http://nick-mbp.local:2345&apos;, params=payload)

print r.url
</command>
    </hudson.plugins.python.Python>
  </builders>
  <publishers>
    <hudson.tasks.Mailer plugin="mailer@1.11">
      <recipients>ngalasso@grubhub.com</recipients>
      <dontNotifyEveryUnstableBuild>false</dontNotifyEveryUnstableBuild>
      <sendToIndividuals>false</sendToIndividuals>
    </hudson.tasks.Mailer>
    <jenkins.plugins.hipchat.HipChatNotifier plugin="hipchat@0.1.5">
      <authToken>9a1c7fa63d3e63b150dd322e8373ab</authToken>
      <buildServerUrl>http://jenkinsmob.grubhubseamless.com:8080/</buildServerUrl>
      <room>T-Rex Nation</room>
      <sendAs>Nicolas</sendAs>
    </jenkins.plugins.hipchat.HipChatNotifier>
  </publishers>
  <buildWrappers/>
</project>
XMLDELIMITER
	end


end
