import requests

payload = {'jobName':'some_job_name',
			'jobStatus':'success'}

r = requests.get('http://nick-mbp.local:2345', params=payload)

print r.url


# BATCH_COMPLETE

#!/usr/bin/python
import os
import requests

payload = {'msg':'BATCH_COMPLETE'}

eleanor = os.environ['ELEANOR_URL']

print 'sending callback to {}'.format(eleanor)

r = requests.get(eleanor, params=payload)

print r.url
