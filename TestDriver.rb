#!/usr/bin/ruby

require 'jenkins_api_client'
require 'enumerator'
require 'socket'

#the hostname and port of the server eleanor will be listening on. 
#used for receiving callbacks when the app is done being built

#nick's test laptop
$eleanorHostame = Socket.gethostbyname(Socket.gethostname).first
$eleanorPort = 23457

# mobile's jenkins box
# $eleanorHostame = 'jenkinsmob.grubhubseamless.com'
# $eleanorPort = 23457

class TestDriver

	@@client = JenkinsApi::Client.new(	:server_ip => '10.92.254.55',
										:server_port => '8080',
		         						:username => 'ruby', 
		         						:password => '8077188bffcce97102bf0fffc004de9f',
		         						# :ssl => false,
		         						:log_level => 0
		         						)
	@@eleanorHost = 'http://' + $eleanorHostame + ':' + $eleanorPort.to_s

	puts 'running eleanor at ' + @@eleanorHost

	@@uniqueID = 'eleanor_sauce'

	nodeList = @@client.node.list(filter='saucelabs_ios_node')
	
	@@slaveNodeCount = @@client.node.get_node_numExecutors(nodeList.first) - 1
	puts "running for #{@@slaveNodeCount} nodes"

	# look up what %w[] means before fucking with this
	@@testNames = %w[ 
					iOS_SmokeTest,
					MATA_14_MenuCategoryPanel_PagedTest,
					MATA_19_MenuSearch_PagedTest,
					MATA_28_MenuScrollAndMenuItemsTest_PagedTest,
					MATA_157_SearchView_PagedTest,
					MATA_191_Login_PagedTest,
					MATA_221_CartShowAll_PagedTest,
					MATA_225_CartDelivery_PagedTest,
					MATA_228_CartPickup_PagedTest,
					MATA_329_CheckoutForPickup_PagedTest,
					MATA_527_DeliveryFlow_PagedTest,
					MATA_531_DeliveryAnonymousFlow_PagedTest,
					MATA_532_ExistingUserExistingAddress_PagedTest,
					MATA_533_ExistingUserNewAddress_PagedTest,
					MATA_612_TutorialViewFirstLaunch_PagedTest,
					MATA_627_TutorialViewSkipFromTray_PagedTest,
					MATA_653_Different_Address_Searches,
					MATA_782_CheckOpenNowIsNotAppliedOnSubsequentSearchesTest,
					MATA_788_NewUserOrderingDeliveryFromDifferentAddressThenGeoLocateOnAddAndEditScreens,
					]

	# @@testNames = ['iOS_SmokeTest']

	@@parentJobName = ''

	def self.buildAppForBranch(branch)

		puts 'building app for branch ' + branch

		template_job_name = 'iOS_sauce_uploader_template'
		xmlString = @@client.job.get_config(template_job_name)

		env = ENV['Environment']
		puts 'building for environment ' + env
		
		params = { 	#'UNIQUEID' => @@uniqueID,
					'ELEANOR_URL' => @@eleanorHost,
					'Branch' => branch,
					'Environment' => env
					}

		@@parentJobName = 'z_' + @@uniqueID + '_parent'

	 	createJob(
				job_name: @@parentJobName, 
				job_xml: xmlString
				)

	 	# buildTests

	 	runJob(job_name: @@parentJobName,
	 			params: params)
	end

	def self.buildTests

		template_job_name = 'SauceLabs_iOS_Template'
		xmlString = @@client.job.get_config(template_job_name)

		#bounds check slaveNodeCount
		if @@slaveNodeCount > @@testNames.count
			@@slaveNodeCount = @@testNames.count
		elsif @@slaveNodeCount < 1
			@@slaveNodeCount = 1
		end
		#OVERRIDE, MAKE EACH TEST ITS OWN JOB
		@@slaveNodeCount = @@testNames.count

		#batch up tests for each node
		batches = @@testNames.in_groups(@@slaveNodeCount, false)

		batches.each_with_index do |testsList, index|
			
			testName = 'z_eleanor_saucelabs_batch_' + "#{index}"
			
			#ridic way to join an array of strings
			testString = testsList * ''

			puts 'creating ' + testName + ' with tests:'
			puts testString

			appiumBranch = ENV['Appium_Branch']
			puts 'appium branch ' + appiumBranch
			
			params = { 
						'TESTS' => testString,
						'UNIQUEID' => @@uniqueID,
						'ELEANOR_URL' => @@eleanorHost,
						'THREAD' => index
						# "Tests_Branch" => appiumBranch
			 		}

			createJob(
				job_name: testName, 
				job_xml: xmlString
				)

		 	runJob(job_name: testName,
		 			params: params)

		end

	end	

	def self.createJob(	job_name: 'job_name' ,
						job_xml: 'xml' 
						)

		createCode = @@client.job.create_or_update(job_name, job_xml)

		raise 'Could not create_or_update the job specified' unless createCode == '200'

	end

	def self.runJob(job_name: 'job_name' ,
					params: 'params'
					)

		buildCode = @@client.job.build(job_name, params)

		raise 'Could not build the job specified' unless buildCode == '201'

	end

	def self.cleanupJobWithName(job_name)
		
		#Jenkins doesn't like it when you delete/recreate jobs with the same name. AVOID - NG
		# deleteCode = @@client.job.delete(job_name)
		# puts deleteCode
		# raise 'Could not delete '+job_name unless deleteCode == '302'

	end

	@@completedTestCount = 0

	def self.handleRequest(params) #TODO: care about concurrency...

		puts params

		message = params['msg']

		if message.include? 'BUILT_APP'

			#Seamless.app is now built, kick off the tests
   			puts 'App built, running tests'
   			buildTests
   			cleanupJobWithName(@@parentJobName)

   		elsif message.include? 'BUILT_SAUCE_APP'

   			#Seamless.app has been uploaded to SauceLabs. Start SauceLabs tests
   			puts 'App built, running tests'
   			buildTests

   		elsif message.include? 'START_TESTS'

   			#this message is not being sent by anything right now
   			#kick off building Seamless.app
   			puts 'building app'

   			branch = params['branch']
   			if branch
   				buildAppForBranch(branch)
   			else
   				buildAppForBranch('integration')
   			end	
   			
   		elsif message.include? 'BATCH_COMPLETE'

   			puts 'a test batch completed'
   			#a batch of tests just completed, increment completed test count
   			@@completedTestCount += 1

   			if @@completedTestCount == @@slaveNodeCount
   				puts 'all tests complete'
   				puts 'eleanor exiting'
   				exit 0
   			else
   				remaingTests = @@slaveNodeCount - @@completedTestCount
   				puts 'waiting for ' + remaingTests.to_s + ' more tests to complete'
   			end

		end

	end

end

class Array

	#stole this code from rails - NG
	#http://apidock.com/rails/Array/in_groups
	def in_groups(number, fill_with = nil)
	    # size.div number gives minor group size;
	    # size % number gives how many objects need extra accommodation;
	    # each group hold either division or division + 1 items.
	    division = size.div number
	    modulo = size % number

	    # create a new array avoiding dup
	    groups = []
	    start = 0

	    number.times do |index|
	      length = division + (modulo > 0 && modulo > index ? 1 : 0)
	      groups << last_group = slice(start, length)
	      last_group << fill_with if fill_with != false &&
	        modulo > 0 && length == division
	      start += length
	    end

	    if block_given?
	      groups.each { |g| yield(g) }
	    else
	      groups
	    end

	  end
end


