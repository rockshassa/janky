require 'socket' # Provides TCPServer and TCPSocket classes
require './TestDriver.rb'
require 'cgi' #used for parsing callbacks

#disable buffering, so output can be visible in jenkins console in real time
$stdout.sync = true 

$cageString = <<DELIM
░░░░░░░░░░░░░░▄▄▄▄▄▄▄▄▄▄▄▄░░░░░░░░░░░░░░
░░░░░░░░░░░░▄████████████████▄░░░░░░░░░░
░░░░░░░░░░▄██▀░░░░░░░▀▀████████▄░░░░░░░░
░░░░░░░░░▄█▀░░░░░░░░░░░░░▀▀██████▄░░░░░░
░░░░░░░░░███▄░░░░░░░░░░░░░░░▀██████░░░░░
░░░░░░░░▄░░▀▀█░░░░░░░░░░░░░░░░██████░░░░
░░░░░░░█▄██▀▄░░░░░▄███▄▄░░░░░░███████░░░
░░░░░░▄▀▀▀██▀░░░░░▄▄▄░░▀█░░░░█████████░░
░░░░░▄▀░░░░▄▀░▄░░█▄██▀▄░░░░░██████████░░
░░░░░█░░░░▀░░░█░░░▀▀▀▀▀░░░░░██████████▄░
░░░░░░░▄█▄░░░░░▄░░░░░░░░░░░░██████████▀░
░░░░░░█▀░░░░▀▀░░░░░░░░░░░░░███▀███████░░
░░░▄▄░▀░▄░░░░░░░░░░░░░░░░░░▀░░░██████░░░
██████░░█▄█▀░▄░░██░░░░░░░░░░░█▄█████▀░░░
██████░░░▀████▀░▀░░░░░░░░░░░▄▀█████████▄
██████░░░░░░░░░░░░░░░░░░░░▀▄████████████
██████░░▄░░░░░░░░░░░░░▄░░░██████████████
██████░░░░░░░░░░░░░▄█▀░░▄███████████████
███████▄▄░░░░░░░░░▀░░░▄▀▄███████████████
DELIM

puts $cageString

# Initialize a TCPServer object that will listen
# on localhost:2345 for incoming connections.
server = TCPServer.new($eleanorHostame, $eleanorPort)

branch = ENV['iOS_Branch']

TestDriver.buildAppForBranch(branch)
# TestDriver.buildTests
# TestDriver.buildApp

# loop infinitely, processing one incoming
# connection at a time.
loop do 

  # Wait until a client connects, then return a TCPSocket
  # that can be used in a similar fashion to other Ruby
  # I/O objects. (In fact, TCPSocket is a subclass of IO.)
  socket = server.accept

  # Read the first line of the request (the Request-Line)
  request = socket.gets

  parts = request.split(' ')

  paramString = parts[1]

  params = CGI::parse(paramString.tr( "/?",''))

  Thread.abort_on_exception = true
  t = Thread.new do
    TestDriver.handleRequest(params)  
  end
  
  # Log the request to the console for debugging
  STDERR.puts request

  response = "eleanor\n"

  # We need to include the Content-Type and Content-Length headers
  # to let the client know the size and type of data
  # contained in the response. Note that HTTP is whitespace
  # sensitive, and expects each header line to end with CRLF (i.e. "\r\n")
  socket.print "HTTP/1.1 200 OK\r\n" +
               "Content-Type: text/plain\r\n" +
               "Content-Length: #{response.bytesize}\r\n" +
               "Connection: close\r\n"

  # Print a blank line to separate the header from the response body,
  # as required by the protocol.
  socket.print "\r\n"

  # Print the actual response body, which is just "Hello World!\n"
  socket.print response

  # Close the socket, terminating the connection
  socket.close

end

#multi threaded server
# require 'socket'                # Get sockets from stdlib

# server = TCPServer.open(2000)   # Socket to listen on port 2000
# loop {                          # Servers run forever
#   Thread.start(server.accept) do |client|
#     client.puts(Time.now.ctime) # Send the time to the client
#   client.puts "Closing the connection. Bye!"
#     client.close                # Disconnect from the client
#   end
# }