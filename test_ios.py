#find the apk file (filename and location are not consistent)
import os
import fnmatch
import itertools
import fileinput
from tempfile import mkstemp
from shutil import move
from os import remove, close

def invokeNode(apkPath):

	delim = ''	

	nodeCmd1 = 'node . -U {} --app {} --command-timeout 540 --log-level error --full-reset &'.format(os.environ['IPHONE_UDID'],apkPath)
	
	#print nodeCmd1

	#os.system() commands create their own environment, which disappears as soon as os.system() returns
	#this means that environmental changes (such as current working directory) do not persist between calls
	#solve this by chaining all calls together in 1 command
	#TODO: do this the 'right' way by using the python subprocess library

	newLine = '\n'
	sequence = ('cd /usr/local/lib/node_modules/appium/', 'pwd', nodeCmd1)
	bashcommands = newLine.join(sequence) 
	print 'invoking node'
	print bashcommands

	os.system(bashcommands)

def invokeMaven(apkPath):

	mavenCmd = 'mvn -Dtest={} -Dghs.test.app={} test'.format(os.environ['TESTS'], apkPath)

	sequence = ('cd {}/appium_repo/Automation/ ;'.format(os.environ['WORKSPACE']),
				mavenCmd)

	delim = '\n'

	bashCmd = delim.join(sequence)

	print 'invoking maven'
	print bashCmd

	os.system(bashCmd)


if __name__ == "__main__":

	print 'doing python voodoo'

	finalPath = '{}/built_app/Seamless.app'.format(os.environ['WORKSPACE'])

	invokeNode(finalPath)

	invokeMaven(finalPath)

	print 'done python voodoo'

