#find the apk file (filename and location are not consistent)
import os
import fnmatch
import itertools
import fileinput
from tempfile import mkstemp
from shutil import move
from os import remove, close

# def replace(file_path, pattern, subst):
# 	#Create temp file
# 	fh, abs_path = mkstemp()
# 	new_file = open(abs_path,'w')
# 	old_file = open(file_path)
# 	for line in old_file:
# 		new_file.write(line.replace(pattern, subst))
# 	#close temp file
# 	new_file.close()
# 	close(fh)
# 	old_file.close()
# 	#Remove original file
# 	remove(file_path)
# 	#Move new file
# 	move(abs_path, file_path)

# def findFiles(pattern, path):
# 	print "findFiles executing for %s" % pattern
# 	for root, dirnames, filenames in os.walk(path):
# 		for filename in fnmatch.filter(filenames, pattern):
# 			filepath = os.path.join(root, filename)
# 			yield filepath;

def invokeNode(apkPath):

	delim = ''	

	nodeCmd1 = 'node . --app \"'
	nodeCmd2 = finalPath
	nodeCmd3 = "\" --command-timeout 540 --platform-version 4.4 --device-name \"samsungGS4\" --full-reset --automation-name Appium --address localhost --session-override --platform-name Android  --log-level error &"
	
	sequence = (nodeCmd1,nodeCmd2,nodeCmd3)
	
	nodeStr = delim.join(sequence)

	print nodeStr

	#os.system() commands create their own environment, which disappears as soon as os.system() returns
	#this means that environmental changes (such as current working directory) do not persist between calls
	#solve this by chaining all calls together in 1 command
	#TODO: do this the 'right' way by using the python subprocess library

	newLine = '\n'

	sequence = ('cd /usr/local/lib/node_modules/appium/', 'pwd', nodeStr)

	bashcommands = newLine.join(sequence) 

	print 'invoking node'
	print bashcommands

	os.system(bashcommands)

def invokeMaven(apkPath):
	#build command
	# export M2_HOME=/usr/local/apache-maven-3.2.3
	# export ANDROID_HOME=/Applications/AndroidStudio.app/sdk
	# export ANDROID_SDK=$ANDROID_HOME
	# PATH=$PATH:$ANDROID_HOME/build-tools
	# PATH=$PATH:$ANDROID_HOME/platform-tools
	# PATH=$PATH:$ANDROID_HOME/tools
	# export PATH=$PATH:$M2_HOME/bin

	# echo printing path
	# echo $PATH

	# whoami
	# cd /Users/Shared/Jenkins/Home/workspace/Espresso_Appium/Automation/ ;
	# mvn -Dtest=${Tests} test
	# OR
	# mvn -Dtest=iOS_SmokeTest -Dghs.test.app=../../appium-run_location/Seamless.app test


	mavenCmd = 'mvn -Dtest={} -Dghs.test.app={} test'.format(os.environ['Tests'], apkPath)
	sequence = ('cd {}/appium_repo/Automation/ ;'.format(os.environ['WORKSPACE']),
				mavenCmd)
	delim = '\n'

	bashCmd = delim.join(sequence)

	print 'invoking maven'
	print bashCmd

	os.system(bashCmd)



if __name__ == "__main__":

	print 'doing python voodoo'

	path = os.getcwd() 
	
	apkExtension = '*.apk'
	apkFileMatches = []
	for root, dirnames, filenames in os.walk(path):
		for filename in fnmatch.filter(filenames, apkExtension):
			apkFileMatches.append(os.path.join(root, filename))
	
	finalPath = ''

	for match in apkFileMatches:
		if 'unaligned' not in match:
			finalPath = match
			print 'found finalPath'

	print finalPath

	invokeNode(finalPath)

	invokeMaven(finalPath)

	print 'done python voodoo'


